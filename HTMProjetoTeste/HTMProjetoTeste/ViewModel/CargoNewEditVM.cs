﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTMProjetoTeste.ViewModel
{
  public class CargoNewEditVM
  {
    public CargoNewEditVM() { }

    public int Id { get; set; }
    public string Descricao { get; set; }
  }
}