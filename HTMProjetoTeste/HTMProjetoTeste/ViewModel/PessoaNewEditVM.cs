﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTMProjetoTeste.ViewModel
{
  public class PessoaNewEditVM
  {
    public PessoaNewEditVM()
    {
      CargoVM = new CargoNewEditVM();
    }

    public int Id { get; set; }
    public string Nome { get; set; }
    public string Sobrenome { get; set; }

    public CargoNewEditVM CargoVM { get; set; }

  }
}