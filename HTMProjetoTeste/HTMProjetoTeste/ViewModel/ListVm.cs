﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTMProjetoTeste.ViewModel
{
  public class ListVM
  {
    public ListVM() { }

    public string Descricao { get; set; }
    public bool? Ativo { get; set; }
  }
}