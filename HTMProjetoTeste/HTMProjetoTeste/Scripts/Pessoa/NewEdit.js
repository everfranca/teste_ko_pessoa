﻿var EntidadeSetor = function () {
    this.Id = null;
    this.Descricao = null;
    this.Sigla = null;
};

getSimpleObjectByUrl = function (url, id) {

    var response = null;
    $.ajax({
        url: url,
        cache: false,
        type: 'GET',
        contentType: 'application/json; charset=utf-8',
        async: false,
        data: (id !== undefined || id !== null) ? 'id=' + id : {},
        success: function (data) {
            response = data;
        }
    });

    return response;
}
var id = null;

var urlArgs = window.location.pathname.split('/');
if (urlArgs.length > 0 && urlArgs.length === 4) {
    id = urlArgs[3];
}

var vm = {};
var response = getSimpleObjectByUrl('/Pessoa/NewEditJson/', id);
vm = ko.mapping.fromJS(response);

vm.Setores = ko.observableArray([]);

var vmSetor = function () {
    var self = this;
    self.Setor = ko.mapping.fromJS(new EntidadeSetor());
    self.SetorJs = ko.computed({
        read: function () {
            return ko.mapping.toJS(self.Setor);
        },
        write: function (value) {
            return ko.mapping.fromJS(value, self.Setor);
        },
        owner: this
    });

    self.IndexEdit = -1;

    self.Clear = function () {
        self.IndexEdit = -1;
        self.SetorJs(new EntidadeSetor());
    };
    self.Clear();

    self.Save = function () {
        if (self.IndexEdit >= 0) {
            vm.Setores.remove(vm.Setores()[self.IndexEdit]);
        }

        vm.Setores.push(self.SetorJs());
        self.Clear();

        $('#frmModal').modal('hide');
    };

    self.New = function () {
        self.Clear();
        $('#frmModal').modal();
    };

    self.Edit = function (idx) {
        self.IndexEdit = idx;
        self.SetorJs(vm.Setores()[idx]);
        $('#frmModal').modal();
    }

    self.Remove = function (idx) {
        vm.Setores.remove(vm.Setores()[idx]);
    }
};

vm.Salvar = function (data, event) {

    $.ajax({
        url: "NewEdit",
        type: "POST",
        data: ko.mapping.toJS(vm),
        dataType: "json",
        async: false,
        success: function (response) {
            alert('Sucesso');
            window.location.href = '/Pessoa/Index';
        }

    });
}

window.vm = vm;

var koSetor = new vmSetor();

ko.applyBindings(koSetor, document.getElementById("_formModal"));
ko.applyBindings(vm, document.getElementById("frmPessoa"))


