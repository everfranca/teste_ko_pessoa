﻿using HTMProjetoTeste.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTMProjetoTeste.Controllers
{
  public class PessoaController : Controller
  {
    // GET: Pessoa
    public ActionResult Index()
    {
      return View();
    }

    public ActionResult NewEdit()
    {
      return View();
    }

    [HttpGet]
    public ActionResult NewEditJson(int? id)
    {
      PessoaNewEditVM pessoaVm = new PessoaNewEditVM();

      if (id.HasValue)
      {
        pessoaVm.Id = 10;
        pessoaVm.Nome = "Andre";
        pessoaVm.Sobrenome = "Oliveira";
        pessoaVm.CargoVM = new CargoNewEditVM() { Id = 1, Descricao = "Programador" };
      }

      return Json(pessoaVm, JsonRequestBehavior.AllowGet);
    }

    [HttpPost]
    public ActionResult List(ListVM listVM)
    {

      List<PessoaNewEditVM> lstPessoa = new List<PessoaNewEditVM>() {
        new PessoaNewEditVM(){  Id = 1, Nome = "Andre", Sobrenome = "Oliveira", CargoVM = new CargoNewEditVM() { Id = 1, Descricao = "Programador" } },
        new PessoaNewEditVM(){ Id = 2, Nome = "Everton", Sobrenome = "França", CargoVM = new CargoNewEditVM() { Id = 2, Descricao = "Programador 1" }},
        new PessoaNewEditVM(){ Id = 3, Nome = "Marcos", Sobrenome = "Diegues", CargoVM = new CargoNewEditVM() { Id = 3 , Descricao = "Programador 2" }}
      };

      return Json(new { Data = lstPessoa });
    }

    [HttpPost]
    public ActionResult NewEdit(PessoaNewEditVM pessoaNewEditVM)
    {
      return Json(new { Data = pessoaNewEditVM });
    }
  }
}